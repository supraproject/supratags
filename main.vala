// FIXME remove dir_path string to a real string[] to avoid string concatenation
void search_dir (string dir_path, StrvBuilder tab_file) throws Error
{
	string name;

	var dir = Dir.open (dir_path, 0);
	while ((name = dir.read_name ()) != null)
	{
		if (name.has_prefix("."))
			continue;
		if (!(name.has_suffix(".c") || name.has_suffix(".h")))
			continue;
		if (name == "compile_commands.json")
			continue;
		string path = Path.build_filename (dir_path, name);
		if (FileUtils.test(path, FileTest.IS_DIR)) {
			search_dir(path, tab_file);
			continue;
		}
		tab_file.add(path);
	}
}

string []get_files_from_dir (string dir_path) {
	StrvBuilder tab_file = new StrvBuilder();
	try {
		search_dir(dir_path, tab_file);
	}
	catch (Error e) {
		printerr (e.message);
	}
	return tab_file.end();
}

string []get_files_from_json (string path) throws Error {
	string contents;
	FileUtils.get_contents("./compile_commands.json", out contents);

	StringBuilder buffer = new StringBuilder.sized(1024);
	unowned string ptr = contents;
	int nb_bracket = 0;

	// NOTE count '{'
	while (true) {
		var begin = ptr.index_of("{");
		if (begin == -1) {
			break;
		}
		nb_bracket++;
		ptr = ptr.offset(begin + 1);
	}
	string []result = new string[nb_bracket];
	nb_bracket = 0;
	ptr = contents;

	while (true) {
		var begin = ptr.index_of("{");
		var end = ptr.index_of("}");

		if (begin == -1 || end == -1) {
			break;
		}

		var begin_directory = ptr.index_of("\"directory\": \"") + 14;
		var end_directory = ptr.index_of("\"", begin_directory);
		var begin_file = ptr.index_of("\"file\": \"") + 9;
		var end_file = ptr.index_of("\"", begin_file);

		var size_directory = end_directory - begin_directory;
		var size_file = end_file - begin_file;

		buffer.insert_len(0, ptr.offset(begin_directory), size_directory);
		buffer.insert_len(size_directory, "/", 1);
		buffer.insert_len(size_directory + 1, ptr.offset(begin_file), size_file);
		buffer.insert_len(size_file + size_directory + 1, "\0", 1);

		result[nb_bracket] = (string)buffer.str;
		++nb_bracket;
		ptr = ptr.offset(end + 1);
	}
	return (result);
}

void create_tags(string[] all_files) throws Error
{
	var tab = new StrvBuilder();
	var? binary = Environment.find_program_in_path("ctags");
	tab.add(binary ?? "ctags-exuberant");
	if (Main.need_print)
		tab.add("-f-");
	else
		tab.add(Main.output_path);
	tab.addv(all_files);
	var res = tab.end();

	foreach (var i in res)
		print("<%s>\n", i);
	print ("\n");
	Process.spawn_sync(null, res, null, SpawnFlags.SEARCH_PATH, null);

	string path_tags;
	if (Main.need_print)
		path_tags = Environment.get_current_dir () + "/tags";
	else
		path_tags = Main.output_path.offset(2);



	string contents;
	FileUtils.get_contents (path_tags, out contents);

	print ("%s\n\n", contents);
	unowned string ptr = contents;
	while (true) {
		if (ptr.has_prefix ("!_TAG_") == false)
			break;
		var end_line = ptr.index_of_char('\n');
		ptr = ptr.offset(end_line + 1);
	}
	print ("%s\n\n", ptr);
	FileUtils.set_contents(path_tags, ptr);

}

void search_path () throws Error {
	unowned var home = Environment.get_home_dir();
	var path = Environment.get_current_dir();
	var i = path.length;
	i--;

	if (path == home)
		return ;
	if (FileUtils.test(path, GLib.FileTest.EXISTS))
	{
		while (home != path && path != "/tmp" && i > 0)
		{
			if (FileUtils.test(path + "/Makefile", FileTest.EXISTS) ||
				FileUtils.test(path + "/src", FileTest.EXISTS) ||
				FileUtils.test(path + "/.git", FileTest.EXISTS))
			{
				// ? if compile_commands.json is found, we stop the search
				if (FileUtils.test(path + "/compile_commands.json", FileTest.EXISTS))
				{
					create_tags (get_files_from_json (path + "/compile_commands.json"));
					return ;
				}
				create_tags(get_files_from_dir(path));
				return ;
			}
			else
			{
				while (path[i] != '/')
					i--;
				path.data[i] = '\0';
			}
		}
		create_tags(get_files_from_dir(Environment.get_current_dir()));
		return ;
	}
}

public class Main : Object
{
	public static string? output_path;
	public static bool need_print = false;
	public static bool silent = false;

	const OptionEntry[] options = {
		{ "output", 'o', OptionFlags.NONE, OptionArg.STRING, ref output_path, "", "Path to the folder" },
		{ "print", '\0', OptionFlags.NONE, OptionArg.NONE, ref need_print, "", "just print without create tags" },
		{ "silent", '\0', OptionFlags.NONE, OptionArg.NONE, ref silent, "", "force silent stdout and stderr" },
		{ null }
	};

	public static void main(string []argv)
	{

		try {
			var opt_context = new OptionContext ();
			opt_context.add_main_entries (options, null);
			opt_context.parse(ref argv);
			if (silent) {
				FileUtils.close(1);
				FileUtils.close(2);
			}
			if (output_path == null)
				output_path = "-f" + Environment.get_current_dir () + "/tags";
			else
				output_path = "-f" + output_path;
			search_path();
		}
		catch (Error e) {
			printerr (e.message);
		}
	}
}

